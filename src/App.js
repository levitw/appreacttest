import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {

  constructor(props) {
      super(props);

      this.mainContent = null;

      this.setMainContentRef = (element) => {
          this.mainContent = element;
      };
  }

  componentDidMount() {
    this.setFocusOnMainContent();
  }

  setFocusOnMainContent() {
    const mainContent = this.mainContent;
    if (mainContent) mainContent.focus();
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>

        <main id="main" role="main" className="main main--flush" ref={this.setMainContentRef} tabIndex="0">
          <div className="framework-spa" data-spa="Checkout">
            <div data-reactroot="" className="hbc-checkout-thankyou__wrapper">
              <div className="hbc-checkout-thankyou__wrapper__text">
              <h2 className="hbc-checkout-thankyou__wrapper__text-heading">ORDER RECEIVED</h2>
              <h4 className="hbc-checkout-thankyou__wrapper__text-thankyou">Thank you for shopping with Saks Fifth Avenue OFF 5TH.</h4>
              <p className="hbc-checkout-thankyou__wrapper__text-details">
              <span><a href="/account/order-history">98012556</a></span></p><p className="hbc-checkout-thankyou__wrapper__text-cancel"><span><a href="/account/order-history">Cancel order</a></span></p><p className="hbc-checkout-thankyou__wrapper__text-customer-service">If you need further assistance, please contact customer service at 1.866.601.5105.</p></div></div></div>
        </main>
      </div>
    );
  }
}

export default App;
